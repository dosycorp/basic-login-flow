const mysql = require('mysql');

const con = mysql.createConnection({
  host: 'localhost',
  user: 'dbuser',
  password: 'password'
});

con.connect(err => {
  if (err) throw err;
  console.log("Connected.");
  con.query("CREATE DATABASE IF NOT EXISTS Users", (err, result) => {
    if (err) throw err;
    console.log("Database created.");
  });
  con.end();
});


