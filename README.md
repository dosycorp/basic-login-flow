# basic-login-flow

A very basic user authorization and session UI flow with NodeJS/MySQL backend.

## Installation

(please note that following assumes the existance of an admin privilege MySQL user with credentials "dbuser:password")

Just run the install script which install MySQL and configures a MySQL database and runs `npm i && npm test`

```shell
$ git clone https://github.com/crislin2046/basic-login-flow.git
$ cd basic-login-flow
$ ./install
```

Then open a browser and point it to `http://localhost:8080`

## If you don't have a MySQL user

You can try something like the following after logging into MySQL:

```SQL
CREATE USER dbuser IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO dbuser WITH GRANT OPTION;
FLUSH PRIVILEGES;
```




