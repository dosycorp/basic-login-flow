USE Users;
DROP TABLE IF EXISTS Sessions;
DROP TABLE IF EXISTS Credentials;

CREATE TABLE IF NOT EXISTS Credentials (
  userid INT NOT NULL AUTO_INCREMENT, 
  handle VARCHAR(20) NOT NULL,
  email VARCHAR(50) NOT NULL,
  password_hash VARCHAR(128) NOT NULL,
  password_salt VARCHAR(128) NOT NULL,
  UNIQUE (handle),
  UNIQUE (email),
  PRIMARY KEY (userid)
);

CREATE TABLE IF NOT EXISTS Sessions (
  sessionid INT NOT NULL AUTO_INCREMENT,
  userid INT NOT NULL,
  cookie VARCHAR(128) NOT NULL,
  start_ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  expires_ts DATETIME,
  UNIQUE (cookie),
  FOREIGN KEY (userid) REFERENCES Credentials(userid),
  PRIMARY KEY (sessionid)
);
