import {T} from './jtype-system/t.js';

const ALPHANUMERIC = /^[a-z0-9A-Z]+$/;
const EMAIL = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

const Handle = T.def(`Handle`, null, {
  verify: i => {
    const validType = T.check(T`String`, i);
    if ( ! validType ) throw `Handle must be a string. It is not.`;
    const validLength = i.length >= 4 && i.length <= 20;
    if ( ! validLength ) throw `Handle must be between 4 and 20 characters.`;
    const validSource = ALPHANUMERIC.test(i);
    if ( ! validSource ) throw `Handle must only contain alphanumeric characters.`;
    const isValid = validType && validLength && validSource;
    return isValid;
  }
});

// Emails are complex (see "Myths programmers believe about Emails" for an idea)
// this is just a simple regex that trades-off between accuracy and usefulness/simplicity.
// /^[^\s@]+@[^\s@]+$/
// but currently I am using the W3C input[type="email"] regex
// from: https://www.w3.org/TR/2012/WD-html-markup-20120315/input.email.html

const Email = T.def(`Email`, null, {
  verify: i => {
    const validType = T.check(T`String`, i);
    if ( ! validType ) throw `Email must be a string. It is not.`;
    const validLength = i.length <= 50;
    if ( ! validLength ) throw `Email must be be less than 50 characters.`;
    const validSyntax = EMAIL.test(i);
    if ( ! validSyntax ) throw `Email must be syntactically correct. It does not match a regular email pattern.`;
    const isValid = validType && validLength && validSyntax;
    return isValid;
  }
});

const Password = T.def(`Password`, null, {
  verify: i => {
    const validType = T.check(T`String`, i);
    if ( ! validType ) throw `Password must be a string. It is not.`;
    const validLength = i.length >= 8 && i.length <= 20;
    if ( ! validLength ) throw `Password must be between 8 and 20 characters.`;
    const isValid = validType && validLength;
    return isValid;
  }
});

const RegistrationAttempt = T.def(`RegistrationAttempt`, {
  handle: T`Handle`,
  email: T`Email`,
  password: T`Password`
});

const LoginAttempt = T.def(`LoginAttempt`, {
  email: T`Email`,
  password: T`Password`
});

const type = {
  Handle, Email, Password,
  RegistrationAttempt, LoginAttempt
};

export default type;


