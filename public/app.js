import {X} from 'https://unpkg.com/brutalist-web/r.js'
import component from 'https://unpkg.com/brutestrap/index.js'
import * as views from './views.js';

let loaded = false;


// start trying to load the app 
  export function start({loggedIn: loggedIn = false} = {}) {
    X`<div class=loading-spinner>Loading JavaScript...${component.spinner()}</div>`.to('.pre-js-load-indicator','replace');
    load({loggedIn});
    document.addEventListener('DOMContentLoaded', load);
  }

// try to load the app and show it when the document is ready
  function load({loggedIn:loggedIn = false} = {}) {
    if ( loaded ) return;
    try {
      views.LandingPage({session:loggedIn}).to('body','beforeEnd');
      setTimeout( () => document.body.classList.add('loaded'), 1000);
      loaded = true;
    } catch(e) {
      setTimeout(load, 400);
    }
  }

// 
  self.addEventListener('hashchange', () => views.ActionStatus());

