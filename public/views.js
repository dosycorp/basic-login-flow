import {R,X} from 'https://unpkg.com/brutalist-web/r.js'
import component from 'https://unpkg.com/brutestrap/index.js'
import {version} from './common.js';

export function LandingPage({
  session: session = undefined
} = {}) {
  return X`
    <main class="app landing">
      ${session ? Logout() : LoginOrRegister() }
    </main>
  `;
}

function LoginOrRegister() {
  return R`
    <ul>
      <li><a class=tab-handle href=#login-tab>Login</a>
      <li><a class=tab-handle href=#register-tab>Register</a>
    </ul>
    ${LoginForm()}
    ${RegisterForm()}
    ${ActionStatus()}
  `;
}

function LoginForm() {
  return R`
    <form id=login-tab 
      class=tab method=POST 
      submit=${requestAndUpdateStatus}
      action=/api/${version}/session/login>
      <fieldset><legend>Login</legend>
        ${ContactField()}
        ${PasswordField()}
        ${FormButtons({value:'login', text:'Login'})}
      </fieldset>
    </form>
  `;
}

function RegisterForm() {
  return R`
    <form id=register-tab 
      class=tab method=POST 
      submit=${requestAndUpdateStatus}
      action=/api/${version}/credential/register>
      <fieldset><legend>Register</legend>
        ${ContactField()}
        ${HandleField()}
        ${PasswordField()}
        ${FormButtons({value:'register', text:'Register'})}
      </fieldset>
    </form>
  `;
}

export function ActionStatus({
   message: message='',
   showLink: showLink = false,
   status: status = 'info'
  }={}) {
  return R`
    <div class="${status} status">
      <p>
        <span class=action-status>${message || X`<!--ok-->`}</span>
      <p>
        ${showLink? R`<a href=${showLink.href}>${showLink.text}</a>` : X`<!--ok-->`}
    </div>
  `;
}

function Logout() {
  return R`
    <form method=POST action=/api/${version}/session/logout>
      ${
        component.button({name:'api-action',value:'logout', type:'submit', text:'Logout'})
      }
    </form>
  `;
}

function ContactField() {
  return component.textInput({
    name:'email',
    required: true,
    type: 'email',
    maxlength: 50,
    size: 25,
    placeholder: 'Email'
  });
}

function HandleField() {
  return component.textInput({
    name:'handle',
    required: true,
    type: 'text',
    minlength: 4,
    maxlength: 20,
    size: 25,
    placeholder: 'Username'
  });
}

function PasswordField() {
  let pwField;

  return component.textInput({
    name:'password',
    type: 'password',
    required: true,
    handlers: { bond: el => pwField = el },
    size: 25,
    minlength: 8,
    maxlength: 20,
    rightElement: component.button({name:'ui-action',type:'button',
      handlers: { click: togglePWFieldVisibility },
      value:'show-password',text:'Show Password'}),
    placeholder: 'Password'
  });

  function togglePWFieldVisibility(e) {
    const button = e.target;
    if ( pwField.type == 'password' ) {
      pwField.type = 'text';
      button.innerText = 'Hide Password';
    } else if ( pwField.type == 'text' ) {
      pwField.type = 'password';
      button.innerText = 'Show Password';
    }
  }
}

function FormButtons({
    value: value = '',
    text: text = '',
  } = {}) {

  if ( ! value ) throw new TypeError(`FormButtons must transmit an api-action for the submit button.`);
  if ( ! text ) text = 'Submit';

  return X`
    ${component.button({name:'api-action',type:'submit',value,text})}
    ${component.button({name:'ui-action',type:'reset',value:'reset',text:'Clear'})}
  `;
}

async function requestAndUpdateStatus(submission) {
  const {target:form} = submission;
  submission.preventDefault();
  const body = new FormData(form);
  const resp = await (await fetch(form.action, {method:form.method,body})).json();
  if ( resp.success ) {
    const showLink = resp.action == 'login' ? {
      text: 'Main Page', href: '/'
    } : {
      text: 'Login', href: '/#login-tab'
    };
    ActionStatus({status: 'success', message: R.skip(`${resp.action} succeeded.`),showLink});
  } else {
    ActionStatus({status: 'failure', 
      message: R.skip(`Failed: ${resp.error || resp.sqlMessage || resp.status}`)});
  }
}

