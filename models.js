import mysql from 'mysql';
import {T} from './public/jtype-system/t.js';
import type from './public/types.js';

const EMAIL_ALREADY_REGISTERED = 'email exists';
const NO_SUCH_EMAIL = 'email does not exist';
const NOT_AUTHORIZED = 'bad login';

const DB_CREDENTIALS = {
  host: 'localhost',
  user: 'dbuser',
  password: 'password',
  database: 'Users'
};

const db = mysql.createConnection(DB_CREDENTIALS);

class Credential {
  static async tryCreate({
    handle, email, password
  } = {}) {
    const registration_attempt = {handle,email,password};
    T.guard(T`RegistrationAttempt`, registration_attempt);

    const password_salt = genRandom();
    const password_hash = hash(password,password_salt);
    const credential = {
      handle, email, password_salt, password_hash
    };

    const existingCredential = await Credential.getByEmail(email);
    if ( !!existingCredential ) {
      const authorized = await Credential.passwordsMatch(existingCredential, password); 
      if ( ! authorized ) {
        throw {error: EMAIL_ALREADY_REGISTERED, status: 401};
      } else return {userid:existingCredential.userid, db_msg: await Credential.update(credential)};
    } else return {newuser:true, db_msg: await Credential.create(credential)};
  }

  static async passwordsMatch(cred, password) {
    return hash(password,cred.password_salt) === cred.password_hash;
  }

  static async getByEmail(email) {
    return new Promise((res,rej) => {
      db.query(`SELECT * from Credentials WHERE email = ?`, email,
        (err, result) => {
        if (err) rej(err);
        else res(result[0]);
      });
    });
  }

  static async update(credential) {
    return new Promise((res,rej) => {
      db.query(`UPDATE Credentials SET ? WHERE userid = ?`, [credential, credential.userid],
        (err, result) => {
          if (err) rej(err);
          else res(result);
        });
    });
  }

  static async create(credential) {
    return new Promise((res,rej) => {
      db.query(`INSERT INTO Credentials SET ?`, credential,
        (err, result) => {
          if (err) rej(err);
          else res(result);
        });
    });
  }

  /*
  // can implement
  static async delete(credential) {} 
  */
}

class Session {
  static async tryCreate({
    email, password
  } = {}) {
    const login_attempt = {email,password};
    T.guard(T`LoginAttempt`, login_attempt);

    const existingCredential = await Credential.getByEmail(email);

    if ( !existingCredential ) throw {error:NO_SUCH_EMAIL, status: 401};

    const authorized = await Credential.passwordsMatch(existingCredential, password);

    if ( !authorized ) throw {error: NOT_AUTHORIZED, status: 401};

    const {userid} = existingCredential;

    const cookie = genRandom();
    const session = {
      userid, cookie
    };

    return {cookie, db_msg: await Session.create(session)};
  }

  static async getByCookie(cookie) {
    return new Promise((res,rej) => {
      db.query(`SELECT * from Sessions WHERE cookie = ?`, cookie,
        (err, result) => {
        if (err) rej(err);
        else res(result[0]);
      });
    });
  }

  static async create(session) {
    return new Promise((res,rej) => {
      db.query(`INSERT INTO Sessions SET ?`, session,
        (err, result) => {
        if (err) rej(err);
        else res(result);
      });
    });
  }

  static async logout(session) {
    session.expires_ts = Date.now() - 1000;
    return new Promise((res,rej) => {
      db.query(`UPDATE Sessions SET ? WHERE sessionid = ?`, [session, session.sessionid],
        (err, result) => {
        if (err) rej(err);
        else res(result);
      });
    });
  }

  /*
  // can implement
  static async delete(session) {} 
  */
}

const models = {
  db, Credential, Session
};

export default models;

function hash(key, salt) {
  // FIXME: security
  // ideally this should use some secure, expensive hashing function 
  // like PBKDF2
  // but to simplify this demo, 
  // I'm just returning the concat
  return key+salt;
}

function genRandom() {
  // FIXME: security
  // again, ideally this should generate secure cookies and salts
  // but for simplicity in this demo I am just using the following
  // "fake" random generator
  return ((+ new Date)*(Math.random()*100)).toString(36);
}

