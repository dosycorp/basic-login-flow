// imports
  import express from 'express';
  import path from 'path';
  import formidable from 'express-formidable';
  import cookieparser from 'cookie-parser';
  import {version} from './public/common.js';
  import models from './models.js';
 
// constants
  const COOKIE_NAME = 'RSDEMO';
  const root = path.join(__dirname, 'public');

// create the HTTP app and a DB connection
  const app = express();

  models.db.connect(err => {
    if (err) throw err;
    console.log("DB connected.");
  });

// add useful middlewares
  app.use(formidable());
  app.use(cookieparser());

  // basic sketch of 'version aware' API  middleware
    app.use('/api/:version/*', async (req,res,next) => {
      if ( req.params.version !== version ) throw new TypeError(`Version ${req.path.version} is not supported on this service.`);
      next();
    });

  // basic sketch of session middleware
    app.use('*', async (req,rex,next) => {
      const loginCookie = req.cookies[COOKIE_NAME];
      const session = loginCookie && await models.Session.getByCookie(loginCookie);
      const loggedIn = session && (!session.expires_ts || new Date(session.expires_ts) < Date.now());
      const sessionState = {loginCookie,session,loggedIn};
      Object.assign(req, sessionState);
      next();
    });

// main API routes for this user registration and sessions service demo
  app.post(`/api/:version/credential/register`, wrap(async (req,res,next) => {   
    const api_msg = await models.Credential.tryCreate(req.fields);
    console.log({api_msg});
    res.type('json').end(JSON.stringify({success:true,action:'register'}));
  }));

  app.post(`/api/:version/session/login`, wrap(async (req,res,next) => {
    const api_msg = await models.Session.tryCreate(req.fields);
    console.log({api_msg});
    if ( api_msg.cookie ) {
      res.cookie(
        COOKIE_NAME, 
        api_msg.cookie, 
        { 
          expires: new Date(Date.now() + 86400000), 
          httpOnly: true,
          sameSite: true
        }
      );
    }
    res.type('json').end(JSON.stringify({success:true,action:'login',mainPage:'/'}));
  }));

  app.post(`/api/:version/session/logout`, wrap(async (req,res,next) => {
    const session = req.session;
    console.log({session});
    const api_msg = await models.Session.logout(session);
    console.log({api_msg});
    if ( api_msg.cookie ) {
      res.cookie(
        COOKIE_NAME, 
        api_msg.cookie, 
        { 
          expires: new Date(Date.now() + 86400000), 
          httpOnly: true,
          sameSite: true
        }
      );
    }
    //res.type('json').send(JSON.stringify({success:true,action:'login'}));
    res.redirect('/#login-tab');
  }));

// main landing page route and sketch of static router
  // notes
    // obviously it's good to do
    // app.use('/', express.static('public/scripts')) or whatever
    // but just to show a bit of manually doing it for this small demo
  app.get('/*', wrap(async (req,res,next) => {
    let fileName;

    if ( req.path == '/' || req.path == '/index.html' ) {
      // it would also be good to do the 'loggedIn' view branching in 
      // the view but that would either require JS access to cookies (not ideal)
      // or SSR views (actually Brutal.js can do this, but my UI Kit Brutestrap, currently cannot)
      // so because of this I am doing the branching here
      fileName = req.loggedIn ? 'loggedin.html' : 'index.html';
    } else {
      fileName = req.path.slice(1);
    }

    // set the content type
    res.type(fileName);

    // send the file or throw an error that can be caught by the global error handler
    await new Promise((resolve,rej) => res.sendFile(fileName, {root}, err => err? rej(err) : resolve(err)));
  }));

// error handling (a global catchall handler)
  app.use((err,req,res,next) => {
    console.error(err);
    const status = err.status || 500;
    res.status(status).type('json').send(`${JSON.stringify(
      err.expose !== false ? err : {error:`${status} error`})}`);
  });

// start the server
  const server = app.listen(8080, () => {
    console.log(`Server up at ${new Date}`);
  });

// capture SIGINT and close DB connection
  process.on('SIGINT', () => {
    server.close();
    models.db.end();
  });

// async middleware for error handling
  function wrap(asyncFn) {
    return (req,res,next) => Promise.resolve(asyncFn(req,res,next)).catch(next);
  }
